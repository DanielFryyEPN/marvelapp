//
//  DetailViewController.swift
//  MarvelApp
//
//  Created by Daniel Freire on 3/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var pokemon: Pokemon?
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = pokemon?.name
        
        let pkService = PokemonService()
        
        pkService.getPokemonImage(id: (pokemon?.id)!) { (pkImage) in
            self.pokemonImageView.image = pkImage
        }
    }
}
