//
//  PokedexViewController.swift
//  MarvelApp
//
//  Created by Daniel Freire on 29/11/17.
//  Copyright © 2017 Daniel Freire. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {

    @IBOutlet weak var pokedexTableView: UITableView!
    
    var pokemonArray: [Pokemon] = []
    var pokemonIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()
    }
    
    func get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }
    
    // MARK:- Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.fillData(pokemon: pokemonArray[indexPath.row])
//        cell.textLabel?.text = pokemonArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DetailViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Section 1"
        default:
            return "Section 2"
        }
    }
}
