//
//  ViewController.swift
//  MarvelApp
//
//  Created by Daniel Freire on 28/11/17.
//  Copyright © 2017 Daniel Freire. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- Actions
    @IBAction func consultarButtonPressed(_ sender: UIButton) {
        
        let pkId = arc4random_uniform(250) + 1
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(pkId)").responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
            }
        }
    }
}

